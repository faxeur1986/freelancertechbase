![freelancertech.png](https://bitbucket.org/repo/pnB8L7/images/1013297804-freelancertech.png)

# Description #
FREELANCERTECH BASE est l'une des applications web J2EE de base FREELANCERTECH ( [www.freelancertech.net] ), architecturée sur trois couches :
La couche web : basée sur le framework JSF avec la librairie de composants graphiques PrimeFaces.
La couche métier ou des services : basée sur le Framework Spring .
La couche d’accès aux données : basée sur le Framework Hibernate gérant la persistance des objets en base de données relationnelle(MySQL, Oracle, PostgreSQL, SQL Server, …).

# Considérations générales #
* Pour toutes les entité, les champs user_cre, user_maj, date_cre et date_maj sont déjà gérés, il n'est donc plus nécessaire de les renseigner à nouveau lors de l'enregistrement ou de la mise à jour.
* Pour tous les champs nécessitant un label, renseigner la propriété id
* Pour les labels, utiliser le composant <p:outputLabel/> en mentionnant sa propriété for

# Base de données #
* Créer un utilisateur mysql avec pour login 'bookstock' et pour mot de passe 'bookstock' également, ceci évitera de modifier le fichier PersistentConfig.java à chque soumission.
* Modifier le fichier WEB-INF/jdbc.properties (Vous devez modifier les variables jdbc.url, jdbc.username,jdbc.password en accord avec les paramètres de votre base de données).
* Le fichier SQL de la base de donnée "freelancertechbase.sql" se trouve dans le répertoire "\src\main\resources" des sources du projet.

![freelancertechbasenetbean2.jpg](https://bitbucket.org/repo/pnB8L7/images/3154896590-freelancertechbasenetbean2.jpg)
![freelancertechbase1.jpg](https://bitbucket.org/repo/pnB8L7/images/3714960555-freelancertechbase1.jpg)
![freelancertechbase3.jpg](https://bitbucket.org/repo/pnB8L7/images/427853957-freelancertechbase3.jpg)