/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.entites;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author User
 */
@Entity
@Table(name = "utilisateur_groupe", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_utilisateur", "id_groupe"})})
public class UtilisateurGroupe extends AbstractEntity implements Serializable, Comparable<UtilisateurGroupe> {

    private static final long serialVersionUID = 1L;
    public static final int DEL_POSITION = 2;
    public static final int ADD_POSITION = 0;
    public static final int MOD_POSITION = 1;

    @Column(name = "actif", nullable = false)
    private Boolean actif;
    @Column(name = "force_")
    private String force;
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Utilisateur.class)
    @JoinColumn(name = "id_utilisateur", referencedColumnName = "id", nullable = false)
    private Utilisateur utilisateur;
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Groupe.class)
    @JoinColumn(name = "id_groupe", referencedColumnName = "id", nullable = false)
    private Groupe groupe;

    public UtilisateurGroupe() {
    }

    public UtilisateurGroupe(Boolean actif, String force, Utilisateur utilisateur, Groupe groupe) {
        this.actif = actif;
        this.force = force;
        this.utilisateur = utilisateur;
        this.groupe = groupe;
    }

    public boolean isAdd() {
        if (force == null) {
            return false;
        }
        return force.charAt(UtilisateurGroupe.ADD_POSITION) == '1';
    }

    public void setAdd(Boolean add) {
        char v = add == true ? '1' : '0';
        if (force == null) {
            force = v + "00";
        }
        char[] tab = force.toCharArray();
        tab[UtilisateurGroupe.ADD_POSITION] = v;
        force = new String(tab);
    }

    public boolean isModif() {
        if (force == null) {
            return false;
        }
        return force.charAt(UtilisateurGroupe.MOD_POSITION) == '1';
    }

    public void setModif(boolean add) {
        char v = add == true ? '1' : '0';
        if (force == null) {
            force = "0" + v + "0";
        }
        char[] tab = force.toCharArray();
        tab[UtilisateurGroupe.MOD_POSITION] = v;
        force = new String(tab);
    }

    public boolean isDel() {
        if (force == null) {
            return false;
        }
        return force.charAt(UtilisateurGroupe.DEL_POSITION) == '1';
    }

    public void setDel(boolean add) {
        char v = add == true ? '1' : '0';
        if (force == null) {
            force = "00" + v;
        }
        char[] tab = force.toCharArray();
        tab[UtilisateurGroupe.DEL_POSITION] = v;
        force = new String(tab);
    }

    public Boolean getActif() {
        return actif;
    }

    public Boolean isActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Groupe getGroupe() {
        return groupe;
    }

    public void setGroupe(Groupe groupe) {
        this.groupe = groupe;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UtilisateurGroupe other = (UtilisateurGroupe) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "UtilisateurGroupe{" + "actif=" + actif + ", force=" + force + ", utilisateur=" + utilisateur + ", groupe=" + groupe + '}';
    }

    /**
     * @return the force
     */
    public String getForce() {
        return force;
    }

    /**
     * @param force the force to set
     */
    public void setForce(String force) {
        this.force = force;
    }

    @Override
    public int compareTo(UtilisateurGroupe other) {
        // Si le parametre est null
        if (other == null) {
            return 1;
        }

        // Si le parametre n'a pas d'ID
        if (other.id == null) {
            return 1;
        }

        // Si l'objet en cours n'a pas d'ID
        if (this.id == null) {
            return -1;
        }

        // Comparaison des IDs
        return this.id.compareTo(other.id);
    }
}
