/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.dao.impl;

import net.freelancertech.base.model.entites.Photo;
import net.freelancertech.base.model.dao.api.PhotoDaoLocal;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jnguetsop
 */
@Repository
public class PhotoDao extends AbstractDao<Photo, Long> implements PhotoDaoLocal {

    public PhotoDao() {
        super(Photo.class);
    }
}
