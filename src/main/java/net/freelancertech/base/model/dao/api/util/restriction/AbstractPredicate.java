package net.freelancertech.base.model.dao.api.util.restriction;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

/**
 * Classe représentant un predicat abstrait
 *
 */
public abstract class AbstractPredicate implements Predicate {

    private static final long serialVersionUID = 1L;

    /**
     * Méthode de construction d'un chemin de propriété à partir de la racine
     *
     * @param <Y>	Paramètre de type du chemin final
     * @param root	Racine de la requete
     * @param stringPath	Chemin sous forme de chaine
     * @return	Chemin recherché sous forme Path
     */
    //@SuppressWarnings("unchecked")
    @SuppressWarnings("unchecked")
    protected <Y extends Comparable<Y>> Path<Y> buildPropertyPath(Root<?> root, String stringPath) {

        // Si la racine est nulle
        if (root == null) {
            return null;
        }

        // Si la chaine est vide
        if ((stringPath == null) || stringPath.trim().isEmpty()) {
            return null;
        }

        // Le Path à retournet
        Path<? extends Comparable<?>> path;

        // On splitte sur le séparateur de champs
        String[] hierarchicalPaths = stringPath.trim().split("\\.");

        // Obtention du premier chemin
        path = root.get(hierarchicalPaths[0]);

        // Si la taille est > 1
        if (hierarchicalPaths.length > 1) {

            // Parcours
            for (int i = 1; i < hierarchicalPaths.length; i++) {

                // Le chemin
                String unitPath = hierarchicalPaths[i];

                // Si le path est vide ou est une suite d'espace
                if ((unitPath == null) || unitPath.trim().isEmpty()) {
                    continue;
                }

                // Acces à la ppt
                path = path.get(unitPath.trim());
            }
        }

        // On retourne le Path
        return (Path<Y>) path;
    }
}
