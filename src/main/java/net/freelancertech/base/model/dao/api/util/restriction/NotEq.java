package net.freelancertech.base.model.dao.api.util.restriction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Classe représentant un predicat de notEq
 *
 * @author <a href="mailto:jetune@yahoo.fr">Jean-Jacques ETUNE NGI</a>
 * @param <Y>
 * @since 26 avr. 2013 : 08:30:19
 */
public class NotEq<Y extends Comparable<Y>> extends AbstractPredicate {

    /**
     * ID Genere Par Eclipse
     */
    private static final long serialVersionUID = 1L;
    /**
     * Nom de la propriete
     */
    protected String property;
    /**
     * Valeur de la Propriete
     */
    protected Y value;

    /**
     * Constructeur avec initialisation des parametres
     *
     * @param property	Nom de la propriete
     * @param value	Valeur de la propriete
     */
    public NotEq(String property, Y value) {
        this.property = property;
        this.value = value;
    }

    /*
     * (non-Javadoc)
     * @see com.bulk.persistence.tools.api.utils.restrictions.Predicate#generateJPAPredicate(javax.persistence.criteria.CriteriaBuilder, javax.persistence.criteria.Root, java.lang.String, java.lang.Comparable)
     */
    @Override
    public Predicate generateJPAPredicate(CriteriaBuilder criteriaBuilder, Root<?> root) {

        // On retourne le predicat
        return criteriaBuilder.notEqual(this.<Y>buildPropertyPath(root, property), value);
    }
}
