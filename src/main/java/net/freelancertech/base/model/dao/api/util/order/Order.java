/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.dao.api.util.order;

/**
 *
 * @author NGUETSOP
 */
public enum Order {

    /**
     * Ordre ascendant.
     */
    ASC,
    /**
     * Ordre descendant.
     */
    DESC;
}
