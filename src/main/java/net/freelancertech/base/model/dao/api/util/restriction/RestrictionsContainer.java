package net.freelancertech.base.model.dao.api.util.restriction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 * Classe representant un conteneur de restrictions
 *
 * @author Jean-Jacques ETUNÈ NGI
 */
public class RestrictionsContainer implements Serializable {

    /**
     * ID Genere par eclipse
     */
    private static final long serialVersionUID = 1L;
    /**
     * Liste des restrictions
     */
    private final List<Predicate> restrictions = new ArrayList<>(0);

    /**
     *
     */
    private RestrictionsContainer() {
    }

    public static RestrictionsContainer newInstance() {
        return new RestrictionsContainer();
    }

    /**
     * Methode d'ajout d'une restriction
     *
     * @param restriction	Restriction a ajouter
     * @return	Conteneur de restrictions
     */
    public RestrictionsContainer addRestriction(Predicate restriction) {

        // Si la restriction est nulle
        if (restriction != null) {
            restrictions.add(restriction);
        }

        // On retourne le container
        return this;
    }

    /**
     * Methode d'ajout de la restriction Eq
     *
     * @param <Y>
     * @param property	Nom de la Propriete
     * @param value	Valeur de la propriete
     * @return	Conteneur
     */
    public <Y extends Comparable<Y>> RestrictionsContainer addEq(String property, Y value) {

        // Ajout de la restriction
        restrictions.add(new Eq<>(property, value));

        // On retourne le conteneur
        return this;
    }
    
     /**
     * Methode d'ajout de la restriction NotEq
     *
     * @param <Y>
     * @param property	Nom de la Propriete
     * @param value	Valeur de la propriete
     * @return	Conteneur
     */
    public <Y extends Comparable<Y>> RestrictionsContainer addNotEq(String property, Y value) {

        // Ajout de la restriction
        restrictions.add(new NotEq<>(property, value));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction GE
     *
     * @param <Y>
     * @param property	Nom de la Propriete
     * @param value	Valeur de la propriete
     * @return	Conteneur
     */
    public <Y extends Comparable<Y>> RestrictionsContainer addGe(String property, Y value) {

        // Ajout de la restriction
        restrictions.add(new Ge<>(property, value));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction GT
     *
     * @param <Y>
     * @param property	Nom de la Propriete
     * @param value	Valeur de la propriete
     * @return	Conteneur
     */
    public <Y extends Comparable<Y>> RestrictionsContainer addGt(String property, Y value) {

        // Ajout de la restriction
        restrictions.add(new Gt<>(property, value));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction Lt
     *
     * @param <Y>
     * @param property	Nom de la Propriete
     * @param value	Valeur de la propriete
     * @return	Conteneur
     */
    public <Y extends Comparable<Y>> RestrictionsContainer addLt(String property, Y value) {

        // Ajout de la restriction
        restrictions.add(new Lt<>(property, value));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction Like
     *
     * @param property	Nom de la Propriete
     * @param value	Valeur de la propriete
     * @return	Conteneur
     */
    public RestrictionsContainer addLike(String property, String value) {

        // Ajout de la restriction
        restrictions.add(new Like(property, value));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction NotLike
     *
     * @param property	Nom de la Propriete
     * @param value	Valeur de la propriete
     * @return	Conteneur
     */
    public RestrictionsContainer addNotLike(String property, String value) {

        // Ajout de la restriction
        restrictions.add(new NotLike(property, value));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction Le
     *
     * @param <Y>
     * @param property	Nom de la Propriete
     * @param value	Valeur de la propriete
     * @return	Conteneur
     */
    public <Y extends Comparable<Y>> RestrictionsContainer addLe(String property, Y value) {

        // Ajout de la restriction
        restrictions.add(new Le<>(property, value));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction IsFalse
     *
     * @param property	Nom de la Propriete
     * @return	Conteneur
     */
    public RestrictionsContainer addIsFalse(String property) {

        // Ajout de la restriction
        restrictions.add(new IsFalse(property));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction IsTrue
     *
     * @param property	Nom de la Propriete
     * @return	Conteneur
     */
    public RestrictionsContainer addIsTrue(String property) {

        // Ajout de la restriction
        restrictions.add(new IsTrue(property));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction IsNotNull
     *
     * @param property	Nom de la Propriete
     * @return	Conteneur
     */
    public RestrictionsContainer addIsNotNull(String property) {

        // Ajout de la restriction
        restrictions.add(new IsNotNull(property));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'ajout de la restriction IsNull
     *
     * @param property	Nom de la Propriete
     * @return	Conteneur
     */
    public RestrictionsContainer addIsNull(String property) {

        // Ajout de la restriction
        restrictions.add(new IsNull(property));

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'obtention de la Liste des restrictions
     *
     * @return Liste des restrictions
     */
    public List<Predicate> getRestrictions() {
        return Collections.unmodifiableList(restrictions);
    }

    /**
     *
     * @param <T>
     * @param cb
     * @param root
     * @return
     */
    public <T> List<javax.persistence.criteria.Predicate> getPredicates(CriteriaBuilder cb, Root<T> root) {
        List<javax.persistence.criteria.Predicate> predicates = new ArrayList<>(restrictions.size());

        for (Predicate predicate : restrictions) {
            predicates.add(predicate.generateJPAPredicate(cb, root));
        }

        return predicates;
    }

    /**
     * Methode d'obtention de la taille du conteneur
     *
     * @return	Taille du conteneur
     */
    public int size() {
        // On retourne la taille de la collection
        return this.restrictions.size();
    }

    /**
     * Méthode permettant de savoir si le conteneur est vide
     *
     * @return true si le conteneur est vide et false sinon
     */
    public boolean isEmpty() {
        return restrictions.isEmpty();
    }

    /**
     * Methode de vidage du conteneur
     */
    public void clear() {
        // Si la collection est non nulle
        if (restrictions != null) {
            restrictions.clear();
        }
    }
}
