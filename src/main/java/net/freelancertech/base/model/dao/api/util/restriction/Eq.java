package net.freelancertech.base.model.dao.api.util.restriction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Classe représentant un predicat de eq
 *
 * @param <Y>
 */
public class Eq<Y extends Comparable<Y>> extends AbstractPredicate {

    /**
     * Nom de la propriete
     */
    protected String property;
    /**
     * Valeur de la Propriete
     */
    protected Y value;

    /**
     * Constructeur avec initialisation des parametres
     *
     * @param property	Nom de la propriete
     * @param value	Valeur de la propriete
     */
    public Eq(String property, Y value) {
        this.property = property;
        this.value = value;
    }

    /*
     * (non-Javadoc)
     * @see com.bulk.persistence.tools.api.utils.restrictions.Predicate#generateJPAPredicate(javax.persistence.criteria.CriteriaBuilder, javax.persistence.criteria.Root)
     */
    @Override
    public Predicate generateJPAPredicate(CriteriaBuilder criteriaBuilder, Root<?> root) { 
        return criteriaBuilder.equal(this.<Y>buildPropertyPath(root, property), value);
    }
}
