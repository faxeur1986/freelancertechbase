/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.dao.impl;

import net.freelancertech.base.model.entites.UtilisateurGroupe;
import net.freelancertech.base.model.dao.api.UtilisateurGroupeDaoLocal;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jnguetsop
 */
@Repository
public class UtilisateurGroupeDao extends AbstractDao<UtilisateurGroupe, Long> implements UtilisateurGroupeDaoLocal {

    public UtilisateurGroupeDao() {
        super(UtilisateurGroupe.class);
    }
}
