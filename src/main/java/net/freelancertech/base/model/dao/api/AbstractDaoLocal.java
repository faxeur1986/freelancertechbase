package net.freelancertech.base.model.dao.api;

import net.freelancertech.base.model.dao.api.exception.DaoException;
import net.freelancertech.base.model.dao.api.util.order.OrderContainer;
import net.freelancertech.base.model.dao.api.util.restriction.RestrictionsContainer;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.criteria.Root;

/**
 * Basic DAO interface.
 *
 * @param <T> Entity type
 * @param <K> Entity's primary key type
 */
public interface AbstractDaoLocal<T extends Serializable, K extends Serializable> {

    /**
     * Find entity by id.
     *
     * @param id Id
     * @return Matched entity
     */
    public T find(final K id);

    /**
     * Find entity by id.
     *
     * @param id Id
     * @param properties Propriétés à charger
     * @return Matched entity
     */
    public T find(final K id, Set<String> properties) throws DaoException;

    /**
     * Permet de rechercher aevc un critère.
     *
     * @param property
     * @param value
     * @return
     */
    public T find(final String property, final Object value);

    public T findOne(RestrictionsContainer rc);

    public T findOne(RestrictionsContainer rc, Set<String> properties);

    /**
     * Permet de rechercher aevc un critère.
     *
     * @param property
     * @param value
     * @param properties Propriétés à charger lors de la requête
     * @return
     */
    public T find(final String property, final Object value, Set<String> properties);

    /**
     * Find all entities of type T.
     *
     * @return List of entities
     */
    public List<T> findAll();

    /**
     * Permet de faire un recherche multicriteres.
     *
     * @param rc
     * @return
     */
    public List<T> find(RestrictionsContainer rc);

    /**
     * Find all entities of type T.
     *
     * @param properties Propriétés à charger en eager
     * @return List of entities
     */
    public List<T> find(Set<String> properties);

    /**
     * Find all entities of type T.
     *
     * @param oc
     * @return List of entities
     */
    public List<T> find(OrderContainer oc);

    /**
     * Permet de faire un recherche multicriteres.
     *
     * @param rc
     * @param properties Propriétés à charger
     * @return
     */
    public List<T> find(RestrictionsContainer rc, Set<String> properties);

    /**
     * Permet de faire un recherche multicriteres.
     *
     * @param rc
     * @param oc
     * @return
     */
    public List<T> find(RestrictionsContainer rc, OrderContainer oc);

    /**
     * Find all entities of type T.
     *
     * @param properties Propriétés à charger en eager
     * @param oc
     * @return List of entities
     */
    public List<T> find(Set<String> properties, OrderContainer oc);

    /**
     * Find all entities of type T.
     *
     * @param rc Rstrictions to be applied.
     * @param properties Properties to be fetched.
     * @param oc Order
     * @return List of entities of type T
     */
    public List<T> find(RestrictionsContainer rc, Set<String> properties, OrderContainer oc);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @return List of entities
     */
    public List<T> findInRange(final int min, final int max);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param rc
     * @return List of entities
     */
    public List<T> findInRange(final int min, final int max, RestrictionsContainer rc);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param properties
     * @return List of entities
     */
    public List<T> findInRange(final int min, final int max, Set<String> properties);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param oc
     * @return List of entities
     */
    public List<T> findInRange(final int min, final int max, OrderContainer oc);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param rc
     * @param properties
     * @return List of entities
     */
    public List<T> findInRange(final int min, final int max, RestrictionsContainer rc, Set<String> properties);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param rc
     * @param oc
     * @return List of entities
     */
    public List<T> findInRange(final int min, final int max, RestrictionsContainer rc, OrderContainer oc);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param properties
     * @param oc
     * @return List of entities
     */
    public List<T> findInRange(final int min, final int max, Set<String> properties, OrderContainer oc);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param rc
     * @param properties
     * @param oc
     * @return List of entities
     */
    public List<T> findInRange(final int min, final int max, RestrictionsContainer rc, Set<String> properties, OrderContainer oc);

    /**
     * Save entity.
     *
     * @param entity Entity to save
     * @return
     */
    public T save(T entity);

    /**
     * Update entity.
     *
     * @param entity Entity to update
     * @return
     * @throws java.lang.Exception
     */
    public T update(T entity) throws Exception;

    /**
     * Delete entity.
     *
     * @param entity Entity to delete
     */
    void delete(T entity);

    /**
     * Delete entity by id.
     *
     * @param id ID
     */
    void deleteById(final K id);

    /**
     * Returns number of entities of type T.
     *
     * @return Number of entities
     */
    public Long count();

    /**
     *
     * @param rc The rectrictions container.
     * @return
     */
    public Long count(RestrictionsContainer rc);

    /**
     *
     * @param oc
     * @return
     */
    public T findLatest(OrderContainer oc);

    /**
     *
     * @param rc
     * @param oc
     * @return
     */
    public T findLatest(RestrictionsContainer rc, OrderContainer oc);

    /**
     *
     * @param root
     * @param properties
     */
    public void addProperties(Root<T> root, Set<String> properties);
}
