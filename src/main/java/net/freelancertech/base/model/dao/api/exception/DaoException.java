/**
 *
 */
package net.freelancertech.base.model.dao.api.exception;

/**
 * @author NGUETSOP
 *
 */
public class DaoException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -1306784431343485231L;

    /**
     *
     */
    public DaoException() {
    }

    /**
     * @param message
     */
    public DaoException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public DaoException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public DaoException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
