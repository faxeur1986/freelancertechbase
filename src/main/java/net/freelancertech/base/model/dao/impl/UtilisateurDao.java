/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.dao.impl;

import net.freelancertech.base.model.entites.Utilisateur;
import net.freelancertech.base.model.dao.api.UtilisateurDaoLocal;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jnguetsop
 */
@Repository
public class UtilisateurDao extends AbstractDao<Utilisateur, Long> implements UtilisateurDaoLocal {

    public UtilisateurDao() {
        super(Utilisateur.class);
    }
}
