package net.freelancertech.base.model.dao.api.util.order;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe representant un conteneur d'ordre de tri des resultats
 *
 */
public class OrderContainer implements Serializable {

    private OrderContainer() {
    }
    /**
     * Liste des ordre de tri
     */
    private final Map<String, Order> orders = new HashMap<>();

    /**
     *
     * @return
     */
    public static OrderContainer getInstance() {
        return new OrderContainer();
    }

    /**
     * Methode d'ajout d'un ordre de tri
     *
     * @param property
     * @param order	Ordre de tri a ajouter
     * @return	Conteneur d'ordre de tri
     */
    public OrderContainer add(String property, Order order) {

        // Si la ppt est nulle
        if ((property == null) || property.trim().isEmpty()) {
            return this;
        }

        // Si le Type d'ordre est null
        if (order == null) {
            return this;
        }

        // Ajout
        orders.put(property.trim(), order);

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'obtention de la Map des ordre de tri
     *
     * @return Map des ordre de tri
     */
    public Map<String, Order> getOrders() {
        return Collections.unmodifiableMap(orders);
    }

    /**
     * Methode d'obtention de la taille du conteneur
     *
     * @return	Taille du conteneur
     */
    public int size() {
        // On retourne la taille de la collection
        return this.orders.size();
    }

    /**
     * Méthoe permettant de savoir si le conteneur est vide
     *
     * @return true si le conteneur est vide et false sinon
     */
    public boolean isEmpty() {
        return orders.isEmpty();
    }

    /**
     * Methode de vidage du conteneur
     */
    public void clear() {
        orders.clear();
    }
}
