/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.dao.impl;

import net.freelancertech.base.model.entites.AbstractEntity;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.FetchParent;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import net.freelancertech.base.model.dao.api.AbstractDaoLocal;
import net.freelancertech.base.model.dao.api.exception.DaoException;
import net.freelancertech.base.model.dao.api.util.order.Order;
import net.freelancertech.base.model.dao.api.util.order.OrderContainer;
import net.freelancertech.base.model.dao.api.util.restriction.Predicate;
import net.freelancertech.base.model.dao.api.util.restriction.RestrictionsContainer;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author NGUETSOP
 * @param <T> - Entity type
 * @param <K> - Entity primary key type
 */
public abstract class AbstractDao<T extends AbstractEntity, K extends Serializable>
        implements AbstractDaoLocal<T, K> {

    @PersistenceContext(unitName = "freelancertechbase_pu")
    private EntityManager em;

    protected Logger logger;// = Logger.getLogger(AbstractDao.class.getName());

    /*
     * @Inject private EntityManager em;
     */
    private final Class<T> clazz;

    /**
     * Constructor
     *
     * @param clazz
     */
    public AbstractDao(final Class<T> clazz) {
        this.clazz = clazz;
        logger = Logger.getLogger(clazz.getName());
    }

    /**
     * @return Entity manager
     */
    public final EntityManager getEntityManager() {
        return em;
    }

    /**
     * Return the runtime type of the entity
     *
     * @return the runtime type of the entity.
     */
    public final Class<T> getType() {
        return clazz;
    }

    /**
     * Save single entity to the database.
     *
     * @param entity entity to be saved.
     * @return return the saved entity whith its id and version.
     */
    @Override
    public final T save(T entity) {
        entity.setUserCre(SecurityContextHolder.getContext().getAuthentication().getName());
        entity.setUserMaj(entity.getUserCre());

        em.persist(entity);
        return entity;
    }

    /**
     * Update an entity.
     *
     * @param entity
     * @return the updated entity whit new version.
     * @throws Exception if there is execption, * * * * * * * * * *
     * particularly, <code>OptimisticLockException</code>.
     * @see javax.persistence.OptimisticLockException
     */
    @Override
    public final T update(T entity) throws Exception {
        entity.setUserMaj(SecurityContextHolder.getContext().getAuthentication().getName());

        try {
            return em.merge(entity);
        } catch (OptimisticLockException exception) {
            logger.log(Level.SEVERE, null, exception);
            throw new DaoException("com.freelancertech.base.dao.update.version.changed");
        }
    }

    /**
     * Delete an entity.
     *
     * @param entity entity to be deleted.
     */
    @Override
    public final void delete(T entity) {
        em.remove(em.merge(entity));
    }

    /**
     * Delete an entity from it id
     *
     * @param id id of the entity to be deleted.
     */
    @Override
    public final void deleteById(final K id) {
        em.remove(em.find(clazz, id));
    }

    /**
     * Find entity by it primary key.
     *
     * @param id id of the entity to be found.
     * @return the entity if found or null if not.
     */
    @Override
    public final T find(final K id) {
        return em.find(clazz, id);
    }

    /**
     * Find entity by it primary key and fetch properstis specified in
     * <code>properties</code>.
     *
     * @param properties
     * @return the entity if found or null if not.
     * @throws DaoException if the id property name cannot be found.
     */
    @Override
    public final T find(final K id, Set<String> properties) throws DaoException {
        String property = getIdPropertyName();
        if ((property == null) || property.trim().isEmpty()) {
            throw new DaoException("com.freelancertech.base.dao.find.id.exists");
        }
        return find(property, id, properties);
    }

    @Override
    public T find(final String property, final Object value) {
        if ((property == null) || property.trim().isEmpty()) {
            throw new DaoException("com.freelancertech.base.dao.find.property.null");
        }

        if (value == null) {
            throw new DaoException("com.freelancertech.base.dao.find.value.null");
        }

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        ParameterExpression<Object> propertyParameter = criteriaBuilder.parameter(Object.class, property);

        criteriaQuery.where(criteriaBuilder.equal(root.get(property.trim()), propertyParameter));

        // Requete basée sur les critères
        TypedQuery<T> query = em.createQuery(criteriaQuery);

        // Positionnement du Paramètre
        query.setParameter(property, value);

        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (NonUniqueResultException nure) {
            throw new DaoException("com.freelancertech.base.dao.find.nure");
        }
    }

    @Override
    public T findOne(RestrictionsContainer rc) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        // Ajout des propriétés à charger en EAGER
        //addProperties(root, properties);
        //Ajout des prédicats
        addRestrictions(rc, criteriaQuery, criteriaBuilder, root);

        // Requete basée sur les critères
        TypedQuery<T> query = em.createQuery(criteriaQuery);

        // Positionnement du Paramètre
        //query.setParameter(property, value);
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (NonUniqueResultException e) {
            throw new DaoException("");
        }
    }

    @Override
    public T findOne(RestrictionsContainer rc, Set<String> properties) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        // Ajout des propriétés à charger en EAGER
        addProperties(root, properties);
        //Ajout des prédicats
        addRestrictions(rc, criteriaQuery, criteriaBuilder, root);

        // Requete basée sur les critères
        TypedQuery<T> query = em.createQuery(criteriaQuery);

        // Positionnement du Paramètre
        //query.setParameter(property, value);
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (NonUniqueResultException e) {
            throw new DaoException("");
        }
    }

    @Override
    public T find(final String property, final Object value, Set<String> properties) {
        if (property == null || property.trim().isEmpty()) {
            throw new DaoException("com.freelancertech.base.dao.find.property.null");
        }

        if (value == null) {
            throw new DaoException("com.freelancertech.base.dao.find.value.null");
        }

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        ParameterExpression<Object> propertyParameter = criteriaBuilder.parameter(Object.class, property);

        criteriaQuery.where(criteriaBuilder.equal(root.get(property.trim()), propertyParameter));

        // Ajout des propriétés à charger en EAGER
        addProperties(root, properties);

        // Requete basée sur les critères
        TypedQuery<T> query = em.createQuery(criteriaQuery);

        // Positionnement du Paramètre
        query.setParameter(property, value);

        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (NonUniqueResultException nure) {
            throw new DaoException("com.freelancertech.base.dao.find.nure");
        }
    }

    @Override
    public final List<T> findAll() {
        CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(clazz);
        cq.select(cq.from(clazz));
        return em.createQuery(cq).getResultList();
    }

    /**
     *
     * @param properties
     * @return
     */
    @Override
    public List<T> find(Set<String> properties) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        // Ajout des propriétés à charger en EAGER
        addProperties(root, properties);

        TypedQuery<T> query = em.createQuery(criteriaQuery);

        return query.getResultList();
    }

    /**
     * Find all entities of type T.
     *
     * @param orderContainer
     * @return List of entities
     */
    @Override
    public List<T> find(OrderContainer orderContainer) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<T> root = criteriaQuery.from(clazz);
        criteriaQuery.select(root);

        addOrders(orderContainer, criteriaQuery, criteriaBuilder, root);

        return em.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<T> find(Set<String> properties, OrderContainer orderContainer) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        // Ajout des propriétés à charger en EAGER
        addProperties(root, properties);

        //Ajout des orders
        addOrders(orderContainer, criteriaQuery, criteriaBuilder, root);

        // Requete basée sur les critères
        TypedQuery<T> query = em.createQuery(criteriaQuery);

        return query.getResultList();
    }

    @Override
    public List<T> find(RestrictionsContainer rc, Set<String> properties, OrderContainer orderContainer) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        // Ajout des propriétés à charger en EAGER
        addProperties(root, properties);

        //Restrictions
        addRestrictions(rc, criteriaQuery, criteriaBuilder, root);

        //Ajout des orders
        addOrders(orderContainer, criteriaQuery, criteriaBuilder, root);

        // Requete basée sur les critères
        TypedQuery<T> query = em.createQuery(criteriaQuery);

        return query.getResultList();
    }

    @Override
    public final List<T> findInRange(final int min, final int max) {
        CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(clazz);
        cq.select(cq.from(clazz));
        TypedQuery<T> tq = em.createQuery(cq);
        tq.setMaxResults(max - min);
        tq.setFirstResult(min);
        return tq.getResultList();
    }

    @Override
    public List<T> findInRange(final int min, final int max, RestrictionsContainer rc) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(clazz);

        Root<T> root = cq.from(clazz);

        cq.select(root);
        //Restrictions
        addRestrictions(rc, cq, cb, root);

        TypedQuery<T> tq = em.createQuery(cq);
        tq.setMaxResults(max - min);

        tq.setFirstResult(min);
        return tq.getResultList();
    }

    @Override
    public final List<T> findInRange(final int min, final int max, Set<String> properties) {
        CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(clazz);
        Root<T> root = cq.from(clazz);

        cq.select(root);

        addProperties(root, properties);

        TypedQuery<T> tq = em.createQuery(cq);
        tq.setMaxResults(max - min);
        tq.setFirstResult(min);

        return tq.getResultList();
    }

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param oc
     * @return List of entities
     */
    @Override
    public List<T> findInRange(final int min, final int max, OrderContainer oc) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = criteriaBuilder.createQuery(clazz);
        Root<T> root = cq.from(clazz);

        cq.select(root);

        addOrders(oc, cq, criteriaBuilder, root);

        TypedQuery<T> tq = em.createQuery(cq);
        tq.setMaxResults(max - min);
        tq.setFirstResult(min);

        return tq.getResultList();
    }

    @Override
    public List<T> findInRange(final int min, final int max, RestrictionsContainer rc, Set<String> properties) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = criteriaBuilder.createQuery(clazz);
        Root<T> root = cq.from(clazz);

        cq.select(root);

        //Properties
        addProperties(root, properties);

        //Restrictions
        addRestrictions(rc, cq, criteriaBuilder, root);

        TypedQuery<T> tq = em.createQuery(cq);
        tq.setMaxResults(max - min);
        tq.setFirstResult(min);

        return tq.getResultList();
    }

    @Override
    public List<T> findInRange(final int min, final int max, RestrictionsContainer rc, OrderContainer oc) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = criteriaBuilder.createQuery(clazz);
        Root<T> root = cq.from(clazz);

        cq.select(root);

        //Restrictions
        addRestrictions(rc, cq, criteriaBuilder, root);

        //Orders
        addOrders(oc, cq, criteriaBuilder, root);

        TypedQuery<T> tq = em.createQuery(cq);
        tq.setMaxResults(max - min);
        tq.setFirstResult(min);

        return tq.getResultList();
    }

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param properties
     * @param oc
     * @return List of entities
     */
    @Override
    public List<T> findInRange(final int min, final int max, Set<String> properties, OrderContainer oc) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = criteriaBuilder.createQuery(clazz);
        Root<T> root = cq.from(clazz);

        cq.select(root);

        addProperties(root, properties);

        addOrders(oc, cq, criteriaBuilder, root);

        TypedQuery<T> tq = em.createQuery(cq);
        tq.setMaxResults(max - min);
        tq.setFirstResult(min);

        return tq.getResultList();
    }

    @Override
    public List<T> findInRange(final int min, final int max, RestrictionsContainer rc, Set<String> properties, OrderContainer oc) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = criteriaBuilder.createQuery(clazz);
        Root<T> root = cq.from(clazz);

        cq.select(root);

        addProperties(root, properties);

        addRestrictions(rc, cq, criteriaBuilder, root);

        addOrders(oc, cq, criteriaBuilder, root);

        TypedQuery<T> tq = em.createQuery(cq);
        tq.setMaxResults(max - min);
        tq.setFirstResult(min);

        return tq.getResultList();
    }

    /**
     * Permet de faire un recherche multicriteres.
     *
     * @param rc
     * @return
     */
    @Override
    public List<T> find(RestrictionsContainer rc) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        //Ajout des prédicats
        addRestrictions(rc, criteriaQuery, criteriaBuilder, root);

        // Requete basée sur les critères
        TypedQuery<T> query = em.createQuery(criteriaQuery);

        return query.getResultList();
    }

    @Override
    public List<T> find(RestrictionsContainer rc, Set<String> properties) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        // Ajout des propriétés à charger en EAGER
        addProperties(root, properties);
        //Ajout des prédicats
        addRestrictions(rc, criteriaQuery, criteriaBuilder, root);

        // Requete basée sur les critères
        TypedQuery<T> query = em.createQuery(criteriaQuery);

        return query.getResultList();
    }

    @Override
    public List<T> find(RestrictionsContainer rc, OrderContainer oc) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);

        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.select(root);

        //Ajout des prédicats
        addRestrictions(rc, criteriaQuery, criteriaBuilder, root);

        //Orders
        addOrders(oc, criteriaQuery, criteriaBuilder, root);
        // Requete basée sur les critères
        TypedQuery<T> query = em.createQuery(criteriaQuery);

        return query.getResultList();
    }

    @Override
    public final Long count() {
        CriteriaQuery<Long> cq = em.getCriteriaBuilder()
                .createQuery(Long.class);
        Root<T> rt = cq.from(clazz);
        cq.select(em.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = em.createQuery(cq);
        try {
            return q.getSingleResult();
        } catch (NoResultException exception) {
            logger.log(Level.SEVERE, null, exception);
            return 0l;
        }
    }

    @Override
    public Long count(RestrictionsContainer restrictionsContainer) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<T> root = criteriaQuery.from(clazz);
        criteriaQuery.select(em.getCriteriaBuilder().count(root));

        addRestrictions(restrictionsContainer, criteriaQuery, criteriaBuilder, root);

        TypedQuery<Long> query = em.createQuery(criteriaQuery);
        try {
            return query.getSingleResult();
        } catch (NoResultException exception) {
            logger.log(Level.SEVERE, null, exception);
            return 0l;
        }
    }

    protected T findOneResult(final String namedQuery, final Map<String, Object> parameters) {
        T result = null;

        try {
            TypedQuery<T> query = em.createNamedQuery(namedQuery, clazz);

            //Query query = em.createNamedQuery(namedQuery);
            // Method that will populate parameters if they are passed not null
            // and empty
            if (parameters != null && !parameters.isEmpty()) {
                populateQueryParameters(query, parameters);
            }

            result = query.getSingleResult();
        } catch (Exception exception) {
            logger.log(Level.SEVERE, "Error while running query", exception);
        }

        return result;
    }

    @Override
    public T findLatest(OrderContainer oc) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = criteriaBuilder.createQuery(clazz);
        Root<T> root = cq.from(clazz);

        cq.select(root);

        addOrders(oc, cq, criteriaBuilder, root);

        TypedQuery<T> tq = getEntityManager().createQuery(cq);
        tq.setMaxResults(1);

        try {
            return tq.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    @Override
    public T findLatest(RestrictionsContainer rc, OrderContainer oc) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = criteriaBuilder.createQuery(clazz);
        Root<T> root = cq.from(clazz);

        cq.select(root);

        addRestrictions(rc, cq, criteriaBuilder, root);

        addOrders(oc, cq, criteriaBuilder, root);

        TypedQuery<T> tq = getEntityManager().createQuery(cq);
        tq.setMaxResults(1);

        try {
            return tq.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    private void populateQueryParameters(Query query,
            Map<String, Object> parameters) {

        parameters.entrySet().stream().forEach((entry) -> {
            query.setParameter(entry.getKey(), entry.getValue());
        });
    }

    @Override
    public void addProperties(Root<T> root, Set<String> properties) {
        if ((properties == null) || properties.isEmpty()) {
            return;
        }

        properties.stream().forEach((String relation) -> {
            FetchParent<T, T> fetch = root;

            StringTokenizer token = new StringTokenizer(relation, ".");

            for (; token.hasMoreTokens();) {
                String pathSegment = token.nextToken();
                fetch = fetch.fetch(pathSegment, JoinType.LEFT);
            }
        });
    }

    /**
     * Return all declared field on a class hierachy up to exclusiveParent if
     * <code>exclusiveParent</code> is not null, otherwise, up to Object.class.
     *
     * @param startClass
     * @param exclusiveParent
     * @return
     */
    private List<Field> getFieldsUpTo(Class<?> startClass,
            Class<?> exclusiveParent) {

        List<Field> currentClassFields = new ArrayList<>(startClass.getDeclaredFields().length);
        currentClassFields.addAll(Arrays.asList(startClass.getDeclaredFields()));

        Class<?> parentClass = startClass.getSuperclass();

        if ((parentClass != null)
                && ((exclusiveParent == null) || !(parentClass.equals(exclusiveParent)))) {
            List<Field> parentClassFields
                    = getFieldsUpTo(parentClass, exclusiveParent);
            currentClassFields.addAll(parentClassFields);
        }

        return currentClassFields;
    }

    /**
     * Return the name of the field annotated with
     * <code>javax.persistence.Id</code> annotation
     *
     * @see javax.persistence.Id
     * @return the name of the id field.
     */
    private String getIdPropertyName() {

        List<Field> dcs = getFieldsUpTo(getType(), Object.class);

        //Field[] declaredFields = getType().getDeclaredFields();
        for (Field field : dcs) {
            logger.log(Level.INFO, "FIELD : {0}", field.getName());
            Annotation[] declaredAnnotations = field.getDeclaredAnnotations();
            for (Annotation annotation : declaredAnnotations) {
                Class<? extends Annotation> annotationType = annotation.annotationType();
                if (annotationType.getClass().equals(Id.class)) {
                    return field.getName();
                }
            }
        }

        return null;
    }

    /**
     * Permet d'ajouter des ordres de tri à une requête.
     *
     * @param oc
     * @param cq
     * @param cb
     * @param root
     */
    protected void addOrders(OrderContainer oc, CriteriaQuery<T> cq,
            CriteriaBuilder cb, Root<T> root) {

        if (oc.isEmpty()) {
            return;
        }

        for (Entry<String, Order> entry : oc.getOrders().entrySet()) {
            if ((entry.getKey() == null) || entry.getKey().trim().isEmpty()
                    || (entry.getValue() == null)) {
                continue;
            }

            if (entry.getValue() == Order.ASC) {
                cq.orderBy(cb.asc(root.get(entry.getKey())));
            } else {
                cq.orderBy(cb.desc(root.get(entry.getKey())));
            }
        }
    }

    protected void addRestrictions(RestrictionsContainer rc, CriteriaQuery<?> criteriaQuery,
            CriteriaBuilder criteriaBuilder, Root<T> root) {
        if (rc.isEmpty()) {
            return;
        }

        List<javax.persistence.criteria.Predicate> predicates = new ArrayList<>(rc.size());

        rc.getRestrictions().stream().forEach((Predicate predicate) -> {
            predicates.add(predicate.generateJPAPredicate(criteriaBuilder, root));
        });

        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new javax.persistence.criteria.Predicate[predicates.size()])));
    }
}
