package net.freelancertech.base.model.dao.api.util.restriction;

import java.io.Serializable;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 * Classe représentant un predicat de sélection.
 *
 * @author NGUETSOP
 */
public interface Predicate extends Serializable {

    /**
     * Methode de construction d'un Predicate JPA 2
     *
     * @param cb	Constructeur de critere
     * @param root	Racine de la requete par critere
     * @return	Predicate JPA 2
     */
    public javax.persistence.criteria.Predicate generateJPAPredicate(CriteriaBuilder cb, Root<?> root);
}
