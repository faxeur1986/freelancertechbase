package net.freelancertech.base.model.dao.api.util.property;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe representant un conteneur de proprietes
 *
 */
public class PropertyContainer implements Serializable {

    /**
     * ID Genere par Eclipse
     */
    private static final long serialVersionUID = 1L;
    /**
     * Ensemble des proprietes
     */
    private final Set<String> properties = new HashSet<>(0);

    private PropertyContainer() {
    }

    public static PropertyContainer newInstance() {
        return new PropertyContainer();
    }

    /**
     * Methode d'ajour d'une propriete
     *
     * @param property	Propriete a ajouter
     * @return	Conteneur de proprietes
     */
    public PropertyContainer add(String property) {
        // Si la propriete n'est pas nulle
        if (property == null || property.trim().isEmpty()) {
            return this;
        }

        properties.add(property);

        // On retourne le conteneur
        return this;
    }

    /**
     * Methode d'obtention de l'Ensemble des proprietes
     *
     * @return Ensemble des proprietes
     */
    public Set<String> getProperties() {
        return Collections.unmodifiableSet(properties);
    }

    /**
     * Methode d'obtention de la taille du conteneur
     *
     * @return	Taille du conteneur
     */
    public int size() {
        // On retourne la taille de la collection
        return this.properties.size();
    }

    /**
     * Méthode permettant de savoir si le conteneur est vide
     *
     * @return true si le conteneur est vide et false sinon
     */
    public boolean isEmpty() {
        return properties.isEmpty();
    }

    /**
     * Methode de vidage du conteneur
     */
    public void clear() {
        // Si la collection est non nulle
        properties.clear();
    }
}
