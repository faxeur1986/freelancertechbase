/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.dao.api;

import net.freelancertech.base.model.entites.Photo;

/**
 *
 * @author NGUETSOP
 */
public interface PhotoDaoLocal extends AbstractDaoLocal<Photo, Long> {
}
