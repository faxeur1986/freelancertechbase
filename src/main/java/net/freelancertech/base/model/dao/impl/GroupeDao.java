/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.dao.impl;

import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.model.dao.api.GroupeDaoLocal;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jnguetsop
 */
@Repository
public class GroupeDao extends AbstractDao<Groupe, Long> implements GroupeDaoLocal {

    public GroupeDao() {
        super(Groupe.class);
    }
}
