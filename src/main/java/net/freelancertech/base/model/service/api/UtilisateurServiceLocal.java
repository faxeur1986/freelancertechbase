/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.service.api;

import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.model.entites.Utilisateur;
import java.util.List;

/**
 *
 * @author jnguetsop
 */
public interface UtilisateurServiceLocal extends AbstractServiceLocal<Utilisateur, Long> {

    Utilisateur connect(String login, String password) throws Exception;

    Utilisateur save(Utilisateur utilisateur, List<Groupe> groupes) throws Exception;

    Utilisateur update(Utilisateur utilisateur, List<Groupe> groupes) throws Exception;
}
