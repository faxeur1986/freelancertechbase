/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.service.impl;

import java.io.Serializable;
import net.freelancertech.base.model.dao.api.AbstractDaoLocal;
import net.freelancertech.base.model.dao.api.UtilisateurGroupeDaoLocal;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import net.freelancertech.base.model.service.api.UtilisateurGroupeServiceLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jnguetsop
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UtilisateurGroupeService extends AbstractService<UtilisateurGroupe, Long> implements UtilisateurGroupeServiceLocal, Serializable {

    @Autowired
    private UtilisateurGroupeDaoLocal utilisateurGroupeDao;

    @Override
    public AbstractDaoLocal<UtilisateurGroupe, Long> getDao() {
        return utilisateurGroupeDao;
    }

    @Override
    public UtilisateurGroupe save(UtilisateurGroupe ug) throws Exception {
        return utilisateurGroupeDao.save(ug);
    }

    @Override
    public UtilisateurGroupe update(UtilisateurGroupe ug) throws Exception {
        return utilisateurGroupeDao.update(ug);
    }

    @Override
    public void delete(UtilisateurGroupe ug) throws Exception {
        utilisateurGroupeDao.delete(ug);
    }

    @Override
    public void deleteById(Long id) throws Exception {
        UtilisateurGroupe ug = getDao().find(id);
        if (ug != null) {
            delete(ug);
        }
    }
}
