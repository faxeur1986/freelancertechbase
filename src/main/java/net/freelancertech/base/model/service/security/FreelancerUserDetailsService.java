/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.service.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.freelancertech.base.model.entites.Utilisateur;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import net.freelancertech.base.model.dao.api.UtilisateurDaoLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NGUETSOP
 */
@Service
@Transactional(readOnly = true)
public class FreelancerUserDetailsService implements UserDetailsService {

    @Autowired
    private UtilisateurDaoLocal utilisateurDao;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        try {
            Set<String> properties = new HashSet<>(2);
            properties.add("ugs.groupe");
            Utilisateur utilisateur = utilisateurDao.find("login", login, properties);

            if (utilisateur == null) {
                throw new Exception("com.freelancertech.base.authentication.usernotfound");
            }

            return new User(utilisateur.getLogin(), utilisateur.getPwd(),
                    utilisateur.isEnabled(), !utilisateur.isExpired(),
                    !utilisateur.isExpired(), utilisateur.isEnabled(),
                    getAuthorities(utilisateur));
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     *
     * @param utilisateur
     * @return
     */
    private List<GrantedAuthority> getAuthorities(Utilisateur utilisateur) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        List<UtilisateurGroupe> ugs = utilisateur.getUgs();

        ugs.stream().forEach((ug) -> {
            authorities.add(new SimpleGrantedAuthority(ug.getGroupe().getNom()));
        });

        return authorities;
    }
}
