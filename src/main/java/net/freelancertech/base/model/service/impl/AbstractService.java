/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import net.freelancertech.base.model.dao.api.exception.DaoException;
import net.freelancertech.base.model.dao.api.util.order.OrderContainer;
import net.freelancertech.base.model.dao.api.util.restriction.RestrictionsContainer;
import net.freelancertech.base.model.service.api.AbstractServiceLocal;

/**
 * Generic bean.
 *
 * @author NGUETSOP
 * @param <T>
 * @param <K>
 */
public abstract class AbstractService<T extends Serializable, K extends Serializable>
        implements AbstractServiceLocal<T, K> {

    @Override
    public T find(K id) {
        return getDao().find(id);
    }

    @Override
    public T find(K id, Set<String> properties) throws DaoException {
        return getDao().find(id, properties);
    }

    @Override
    public T find(String property, Object value) {
        return getDao().find(property, value);
    }

    @Override
    public T find(String property, Object value, Set<String> properties) {
        return getDao().find(property, value, properties);
    }

    @Override
    public List<T> findAll() {
        return getDao().findAll();
    }

    @Override
    public List<T> find(Set<String> properties) {
        return getDao().find(properties);
    }

    @Override
    public List<T> find(OrderContainer oc) {
        return getDao().find(oc);
    }

    @Override
    public List<T> find(Set<String> properties, OrderContainer oc) {
        return getDao().find(properties, oc);
    }

    @Override
    public List<T> find(int min, int max) {
        return getDao().findInRange(min, max);
    }

    @Override
    public List<T> find(int min, int max, Set<String> properties) {
        return getDao().findInRange(min, max, properties);
    }

    @Override
    public List<T> find(int min, int max, OrderContainer oc) {
        return getDao().findInRange(min, max, oc);
    }

    @Override
    public List<T> find(int min, int max, Set<String> properties, OrderContainer oc) {
        return getDao().findInRange(min, max, properties, oc);
    }

    @Override
    public List<T> find(RestrictionsContainer rc) {
        return getDao().find(rc);
    }

    @Override
    public List<T> find(RestrictionsContainer rc, Set<String> properties) {
        return getDao().find(rc, properties);
    }

    @Override
    public Long count() {
        return getDao().count();
    }

    @Override
    public Long count(RestrictionsContainer rc) {
        return getDao().count(rc);
    }

    @Override
    public T findLatest(OrderContainer oc) {
        return getDao().findLatest(oc);
    }

    @Override
    public T findLatest(RestrictionsContainer rc, OrderContainer oc) {
        return getDao().findLatest(rc, oc);
    }
}
