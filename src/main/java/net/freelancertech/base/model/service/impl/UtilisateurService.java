/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.service.impl;

import net.freelancertech.base.model.entites.Groupe;
import java.io.Serializable;
import net.freelancertech.base.model.entites.Utilisateur;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import net.freelancertech.base.model.dao.api.AbstractDaoLocal;
import net.freelancertech.base.model.dao.api.GroupeDaoLocal;
import net.freelancertech.base.model.dao.api.UtilisateurDaoLocal;
import net.freelancertech.base.model.dao.api.UtilisateurGroupeDaoLocal;
import net.freelancertech.base.model.dao.api.util.restriction.RestrictionsContainer;
import net.freelancertech.base.model.service.api.UtilisateurServiceLocal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jnguetsop
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UtilisateurService extends AbstractService<Utilisateur, Long> implements UtilisateurServiceLocal, Serializable {

    @Autowired
    private UtilisateurDaoLocal utilisateurDao;
    @Autowired
    private GroupeDaoLocal groupeDao;
    @Autowired
    private UtilisateurGroupeDaoLocal utilisateurGroupeDao;

    @Override
    public AbstractDaoLocal<Utilisateur, Long> getDao() {
        return utilisateurDao;
    }

    @Override
    public Utilisateur save(Utilisateur utilisateur, List<Groupe> groupes) throws Exception {
        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("login", utilisateur.getLogin());

        Utilisateur u = utilisateurDao.findOne(rc);
        if (u != null) {
            throw new Exception("Un autre utilisateur a déjà ce login : " + utilisateur.getLogin());
        }

        PasswordEncoder encoder = new BCryptPasswordEncoder(8);
        utilisateur.setPwd(encoder.encode(utilisateur.getPwd()));

        utilisateur = utilisateurDao.save(utilisateur);
        for (Groupe groupe : groupes) {
            UtilisateurGroupe ug = new UtilisateurGroupe(Boolean.TRUE, "", utilisateur, groupe);
            utilisateurGroupeDao.save(ug);
        }

        return utilisateur;
    }

    @Override
    public Utilisateur save(Utilisateur utilisateur) throws Exception {
        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("login", utilisateur.getLogin());

        Utilisateur u = utilisateurDao.findOne(rc);
        if (u != null) {
            throw new Exception("Un autre utilisateur a déjà ce login : " + utilisateur.getLogin());
        }

        PasswordEncoder encoder = new BCryptPasswordEncoder(8);
        utilisateur.setPwd(encoder.encode(utilisateur.getPwd()));

        return utilisateurDao.save(utilisateur);
    }

    @Override
    public Utilisateur update(Utilisateur utilisateur) throws Exception {
        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("login", utilisateur.getLogin());
        Utilisateur u = utilisateurDao.findOne(rc);

        if ((u != null) && (!utilisateur.getId().equals(u.getId()))) {
            throw new Exception("Un autre utilisateur a déjà ce login : " + utilisateur.getLogin());
        }

        return utilisateurDao.update(utilisateur);
    }

    @Override
    public Utilisateur update(Utilisateur utilisateur, List<Groupe> groupes) throws Exception {
        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("login", utilisateur.getLogin());
        Utilisateur u = utilisateurDao.findOne(rc);

        if ((u != null) && (!utilisateur.getId().equals(u.getId()))) {
            throw new Exception("Un autre utilisateur a déjà ce login : " + utilisateur.getLogin());
        }

        utilisateur = utilisateurDao.update(utilisateur);
        
        rc.clear();
        rc.addEq("utilisateur.id", utilisateur.getId());

        Set<String> properties = new HashSet<>(1);
        properties.add("groupe");

        List<UtilisateurGroupe> ugs = utilisateurGroupeDao.find(rc, properties);
        List<Groupe> gs = new ArrayList<>(ugs.size());

        for (UtilisateurGroupe ug : ugs) {
            gs.add(ug.getGroupe());

            if (!groupes.contains(ug.getGroupe())) {
                utilisateurGroupeDao.delete(ug);
            }
        }

        for (Groupe groupe : groupes) {
            if (!gs.contains(groupe)) {
                UtilisateurGroupe ug = new UtilisateurGroupe(Boolean.TRUE, "", utilisateur, groupe);
                utilisateurGroupeDao.save(ug);
            }
        }

        return utilisateur;
    }

    @Override
    public void delete(Utilisateur utilisateur) throws Exception {
        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("utilisateur.id", utilisateur.getId());
        List<UtilisateurGroupe> ugs = utilisateurGroupeDao.find(rc);

        ugs.stream().forEach((ug) -> {
            utilisateurGroupeDao.delete(ug);
        });

        utilisateurDao.delete(utilisateur);
    }

    @Override
    @Transactional(readOnly = true)
    public Utilisateur connect(String login, String password) throws Exception {
        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("login", login);
        Utilisateur utilisateur = utilisateurDao.findOne(rc);

        if ((utilisateur != null) && (utilisateur.getPwd().equals(password))) {
            if (!utilisateur.isEnabled()) {
                throw new Exception("Echec de connexion : Votre compte est suspendu.");
            }
            return utilisateur;
        }
        return null;
    }

    @Override
    public void deleteById(Long id) throws Exception {
        Utilisateur utilisateur = getDao().find(id);
        if (utilisateur != null) {
            delete(utilisateur);
        }
    }
}
