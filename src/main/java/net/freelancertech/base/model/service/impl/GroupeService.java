/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.service.impl;

import java.io.Serializable;
import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.model.dao.api.AbstractDaoLocal;
import net.freelancertech.base.model.dao.api.GroupeDaoLocal;
import net.freelancertech.base.model.dao.api.util.restriction.RestrictionsContainer;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import net.freelancertech.base.model.service.api.GroupeServiceLocal;
import net.freelancertech.base.model.service.api.UtilisateurGroupeServiceLocal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jnguetsop
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class GroupeService extends AbstractService<Groupe, Long> implements GroupeServiceLocal, Serializable {

    @Autowired
    private GroupeDaoLocal groupeDao;
    @Autowired
    private UtilisateurGroupeServiceLocal utilisateurGroupeService;

    @Override
    public AbstractDaoLocal<Groupe, Long> getDao() {
        return groupeDao;
    }

    @Override
    public Groupe save(Groupe groupe) throws Exception {
        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("nom", groupe.getNom());
        Groupe g = groupeDao.findOne(rc);
        if (g != null) {
            throw new Exception("Un autre groupe a déjà ce nom : " + groupe.getNom());
        }

        rc.clear();
        rc.addEq("libelle", groupe.getLibelle());
        g = groupeDao.findOne(rc);
        if (g != null) {
            throw new Exception("Un autre groupe a déjà ce libellé : " + groupe.getLibelle());
        }

        return groupeDao.save(groupe);
    }

    @Override
    public Groupe update(Groupe groupe) throws Exception {
        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("nom", groupe.getNom());
        Groupe g = groupeDao.findOne(rc);

        if ((g != null) && (!groupe.getId().equals(g.getId()))) {
            throw new Exception("Un autre groupe a déjà ce nom : " + groupe.getNom());
        }

        rc.clear();
        rc.addEq("libelle", groupe.getLibelle());
        g = groupeDao.findOne(rc);

        if ((g != null) && (!groupe.getId().equals(g.getId()))) {
            throw new Exception("Un autre groupe a déjà ce libelle : " + groupe.getLibelle());
        }

        return groupeDao.update(groupe);
    }

    @Override
    public void delete(Groupe groupe) throws Exception {
        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("groupe", groupe);
        List<UtilisateurGroupe> ugs = utilisateurGroupeService.find(rc);

        if ((ugs != null) && !ugs.isEmpty()) {
            throw new Exception("Il existe des utilisateurs appartenant à ce groupe.");
        }

        groupeDao.delete(groupe);
    }

    @Override
    public void deleteById(Long id) throws Exception {
        Groupe groupe = getDao().find(id);
        if (groupe != null) {
            delete(groupe);
        }
    }
}
