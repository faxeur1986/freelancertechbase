/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.service.api;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import net.freelancertech.base.model.dao.api.AbstractDaoLocal;
import net.freelancertech.base.model.dao.api.exception.DaoException;
import net.freelancertech.base.model.dao.api.util.order.OrderContainer;
import net.freelancertech.base.model.dao.api.util.restriction.RestrictionsContainer;

/**
 *
 * @author NGUETSOP
 * @param <T> - Entity type
 * @param <K> - Entity's primary key type
 */
public interface AbstractServiceLocal<T extends Serializable, K extends Serializable> {

    public abstract AbstractDaoLocal<T, K> getDao();

    /**
     * Find entity by id.
     *
     * @param id Id
     * @return Matched entity
     */
    T find(final K id);

    /**
     * Find entity by id.
     *
     * @param id Id
     * @param properties Propriétés à charger en eager
     * @return Matched entity
     */
    T find(final K id, Set<String> properties) throws DaoException;

    /**
     * Permet de rechercher aevc un critère.
     *
     * @param property
     * @param value
     * @return
     */
    T find(final String property, final Object value);

    /**
     * Permet de rechercher aevc un critère.
     *
     * @param property
     * @param value
     * @param properties Propriétés à charger lors de la requête
     * @return
     */
    T find(final String property, final Object value, Set<String> properties);

    /**
     * Find all entities of type T.
     *
     * @return List of entities
     */
    List<T> findAll();

    /**
     * Find all entities of type T.
     *
     * @param properties Propriétés à charger en eager
     * @return List of entities
     */
    List<T> find(Set<String> properties);

    /**
     * Find all entities of type T.
     *
     * @param oc
     * @return List of entities
     */
    List<T> find(OrderContainer oc);

    /**
     * Find all entities of type T.
     *
     * @param properties Propriétés à charger en eager
     * @param orderContainer
     * @return List of entities
     */
    List<T> find(Set<String> properties, OrderContainer orderContainer);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @return List of entities
     */
    public List<T> find(final int min, final int max);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param properties
     * @return List of entities
     */
    public List<T> find(final int min, final int max, Set<String> properties);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param orderContainer
     * @return List of entities
     */
    public List<T> find(final int min, final int max, OrderContainer orderContainer);

    /**
     * Find entities in range min and max
     *
     * @param min
     * @param max
     * @param properties
     * @param orderContainer
     * @return List of entities
     */
    public List<T> find(final int min, final int max, Set<String> properties, OrderContainer orderContainer);

    /**
     * Permet de faire un recherche multicriteres.
     *
     * @param rc
     * @return
     */
    public List<T> find(RestrictionsContainer rc);

    /**
     * Permet de faire un recherche multicriteres.
     *
     * @param rc
     * @param properties Propriétés à charger
     * @return
     */
    public List<T> find(RestrictionsContainer rc, Set<String> properties);

    /**
     * Save entity.
     *
     * @param entity Entity to save
     * @return
     * @throws java.lang.Exception
     */
    public default T save(T entity) throws Exception {
        return getDao().save(entity);
    }

    /**
     * Update entity.
     *
     * @param entity Entity to update
     * @return
     * @throws java.lang.Exception
     */
    public default T update(T entity) throws Exception {
        return getDao().update(entity);
    }

    /**
     * Delete entity.
     *
     * @param entity Entity to delete
     * @throws java.lang.Exception
     */
    public default void delete(T entity) throws Exception {
        getDao().delete(entity);
    }

    /**
     * Delete entity by id.
     *
     * @param id ID
     * @throws java.lang.Exception
     */
    public default void deleteById(final K id) throws Exception {
        getDao().deleteById(id);
    }

    /**
     * Returns number of entities of type T.
     *
     * @return Number of entities
     */
    Long count();

    /**
     *
     * @param rc
     * @return
     */
    Long count(RestrictionsContainer rc);

    /**
     *
     * @param oc
     * @return
     */
    T findLatest(OrderContainer oc);

    /**
     *
     * @param rc
     * @param oc
     * @return
     */
    T findLatest(RestrictionsContainer rc, OrderContainer oc);
}
