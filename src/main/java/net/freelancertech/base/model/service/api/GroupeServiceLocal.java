/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.service.api;

import net.freelancertech.base.model.entites.Groupe;

/**
 *
 * @author jnguetsop
 */
public interface GroupeServiceLocal extends AbstractServiceLocal<Groupe, Long> {
}
