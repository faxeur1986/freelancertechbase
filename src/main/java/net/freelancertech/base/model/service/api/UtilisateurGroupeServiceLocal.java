/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.service.api;

import net.freelancertech.base.model.entites.UtilisateurGroupe;

/**
 *
 * @author jnguetsop
 */
public interface UtilisateurGroupeServiceLocal extends AbstractServiceLocal<UtilisateurGroupe, Long> {
}
