/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.view.converter;

import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.model.service.api.GroupeServiceLocal;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author auguste
 */
@Named
@ApplicationScoped
@FacesConverter(forClass = Groupe.class, value = "com.freelancertech.base.view.converter.GroupeConverter")
public class GroupeConverter implements Converter {

    @Autowired
    private GroupeServiceLocal groupeService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        return groupeService.find("libelle", value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Groupe groupe;
        if (value instanceof Groupe) {
            groupe = (Groupe) value;
            return groupe.getLibelle();
        }
        return "";
    }
}
