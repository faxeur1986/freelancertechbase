package net.freelancertech.base.view.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.RequestScoped;

import javax.faces.context.FacesContext;

import javax.inject.Named;

/**
 *
 * @author fouomene
 */

@Named
@RequestScoped
public class LogoutController implements Serializable {

     private static final String HOME_URL = "index.jsf?faces-redirect=true";

    public void doLogout() throws IOException {
        //TODO : Modifier la ligne ci-dessous
        //SecurityUtils.getSubject().logout();
        FacesContext.getCurrentInstance()
                .getExternalContext().invalidateSession();
        FacesContext.getCurrentInstance()
                .getExternalContext().redirect(HOME_URL);
    }

}
