package net.freelancertech.base.view.controller;

import net.freelancertech.base.model.entites.Groupe;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import net.freelancertech.base.model.entites.Utilisateur;
import net.freelancertech.base.view.util.FacesUtils;
import net.freelancertech.base.model.dao.api.util.order.Order;
import net.freelancertech.base.model.dao.api.util.order.OrderContainer;
import net.freelancertech.base.model.dao.api.util.restriction.RestrictionsContainer;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import net.freelancertech.base.model.service.api.GroupeServiceLocal;
import net.freelancertech.base.model.service.api.UtilisateurGroupeServiceLocal;
import net.freelancertech.base.model.service.api.UtilisateurServiceLocal;
import net.freelancertech.base.util.ExceptionUtil;
import java.util.HashSet;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author jnguetsop
 */
@Named
@SessionScoped
public class UtilisateurController extends AbstractController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Autowired
    private UtilisateurServiceLocal utilisateurService;
    @Autowired
    private GroupeServiceLocal groupeService;
    @Autowired
    private UtilisateurGroupeServiceLocal utilisateurGroupeService;

    private Utilisateur utilisateur;
    private List<Utilisateur> utilisateurs;
    private List<Utilisateur> filtereds;

    private DualListModel<Groupe> pickListGroupes;
    private List<Groupe> targetGroupes;

    /**
     * Default constructor
     */
    public UtilisateurController() {
        utilisateurs = new ArrayList<>(0);
        filtereds = new ArrayList<>(0);
        targetGroupes = new ArrayList<>(0);
        pickListGroupes = new DualListModel<>();
    }

    @PostConstruct
    public void init() {
        initEntity();
    }

    @Override
    public void initEntity() {
        utilisateur = new Utilisateur();
        List<Groupe> gs = groupeService.find(OrderContainer.getInstance().add("libelle", Order.ASC));
        targetGroupes = new ArrayList<>(0);
        pickListGroupes = new DualListModel<>(gs, new ArrayList<Groupe>(0));
    }

    @Override
    public String performCreate() {
        try {
            utilisateur = utilisateurService.save(utilisateur, pickListGroupes.getTarget());
            FacesUtils.addInfo("Utilisateur enrégistré avec succès : " + utilisateur.getLogin());
            init();

            return "index" + facesRedirect;
        } catch (Exception exception) {
            String message = ExceptionUtil.getRootErrorMessage(exception,
                    "Echec lors de l'enregistrement.");
            FacesUtils.addError(this.getClass(), exception, message);
        }

        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String edit() {
        if (utilisateur == null) {
            FacesUtils.addError("Veuillez sélectionner un groupe.");
            return null;
        }

        utilisateur = utilisateurService.find(utilisateur.getId());

        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("utilisateur", utilisateur);

        Set<String> properties = new HashSet<>(1);
        properties.add("groupe");
        List<UtilisateurGroupe> ugs = utilisateurGroupeService.find(rc, properties);
        targetGroupes = new ArrayList<>(ugs.size());

        ugs.stream().forEach((ug) -> {
            targetGroupes.add(ug.getGroupe());
        });

        return super.edit();
    }

    @Override
    public String performEdit() {
        try {
            utilisateur = utilisateurService.update(utilisateur, pickListGroupes.getTarget());
            if ((utilisateurs != null) && utilisateurs.contains(utilisateur)) {
                utilisateurs.set(utilisateurs.indexOf(utilisateur), utilisateur);
            }

            FacesUtils.addInfo("L'utilisateur \"" + utilisateur.getLogin() + "\" a été modifié avec succès!");
            init();

            return "index" + facesRedirect;
        } catch (Exception exception) {
            String message = ExceptionUtil.getRootErrorMessage(exception,
                    "Update failed. See server log for more information");
            FacesUtils.addError(this.getClass(), exception, message);
        }

        return null;
    }

    @Override
    public String view() {
        if (utilisateur == null) {
            FacesUtils.addError("Veuillez sélectionner un utilisateur.");
            return null;
        }

        utilisateur = utilisateurService.find(utilisateur.getId());

        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("utilisateur", utilisateur);

        Set<String> properties = new HashSet<>(1);
        properties.add("groupe");

        List<UtilisateurGroupe> ugs = utilisateurGroupeService.find(rc, properties);
        targetGroupes = new ArrayList<>(ugs.size());

        ugs.stream().forEach((ug) -> {
            targetGroupes.add(ug.getGroupe());
        });

        return super.view();
    }

    /**
     *
     * @return
     */
    @Override
    public String delete() {
        if (utilisateur == null) {
            FacesUtils.addError("Veuillez sélectionner un utilisateur.");
            return null;
        }

        utilisateur = utilisateurService.find(utilisateur.getId());

        RestrictionsContainer rc = RestrictionsContainer.newInstance();
        rc.addEq("utilisateur", utilisateur);

        Set<String> properties = new HashSet<>(1);
        properties.add("groupe");

        List<UtilisateurGroupe> ugs = utilisateurGroupeService.find(rc, properties);
        targetGroupes = new ArrayList<>(ugs.size());

        ugs.stream().forEach((ug) -> {
            targetGroupes.add(ug.getGroupe());
        });

        return super.delete();
    }

    /**
     *
     * @return
     */
    @Override
    public String performDelete() {
        try {
            utilisateurService.delete(utilisateur);
            FacesUtils.addInfo("L'utilisateur \"" + utilisateur.getLogin() + "\" a été supprimé avec succès.");

            if ((utilisateurs != null) && utilisateurs.contains(utilisateur)) {
                utilisateurs.remove(utilisateur);
            }

            init();

            return "index" + facesRedirect;
        } catch (Exception exception) {
            String message = ExceptionUtil.getRootErrorMessage(exception,
                    "Delete failed. See server log for more information");
            FacesUtils.addError(this.getClass(), exception, message);
        }

        return null;
    }

    public String pdfList() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public String excelList() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public String jpeg() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public String pdf() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    //Getters and Setters
    public List<Utilisateur> getUtilisateurs() {
        OrderContainer oc = OrderContainer.getInstance();
        oc.add("login", Order.ASC)
                .add("nom", Order.ASC);
        utilisateurs = utilisateurService.find(oc);

        return utilisateurs;
    }

    public void load(ActionEvent event) {
        getUtilisateurs();
    }

    public void setUtilisateurs(List<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur groupe) {
        this.utilisateur = groupe;
    }

    public List<Utilisateur> getFiltereds() {
        return filtereds;
    }

    public void setFiltereds(List<Utilisateur> filtereds) {
        this.filtereds = filtereds;
    }

    public DualListModel<Groupe> getPickListGroupes() {
        List<Groupe> groupes = groupeService.findAll();
        if (targetGroupes != null) {
            groupes.removeAll(targetGroupes);
        }

        pickListGroupes.setSource(groupes);
        pickListGroupes.setTarget(targetGroupes == null ? (new ArrayList<>(0)) : targetGroupes);
        return pickListGroupes;
    }

    public void setPickListGroupes(DualListModel<Groupe> pickListGroupes) {
        this.pickListGroupes = pickListGroupes;
    }

    public void onTransfer(TransferEvent event) {
        StringBuilder builder = new StringBuilder();
        event.getItems().stream().forEach((item) -> {
            builder.append(((Groupe) item).getLibelle()).append("<br />");
        });

        FacesMessage msg = new FacesMessage();
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        msg.setSummary("Items Transferred");
        msg.setDetail(builder.toString());

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
