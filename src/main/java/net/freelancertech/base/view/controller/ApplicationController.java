package net.freelancertech.base.view.controller;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import net.freelancertech.base.util.Constantes;

@Named
@ApplicationScoped
public class ApplicationController implements Serializable {

    @PostConstruct
    public void init() {
        System.out.println("\n\n\n\n\nEntering init\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\nLeaving init\n\n\n\n\n\n");
    }

    private static final long serialVersionUID = 1L;

    public String getApplicationName() {
        return Constantes.APPLICATION_NAME;
    }
}
