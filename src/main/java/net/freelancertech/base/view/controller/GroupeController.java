package net.freelancertech.base.view.controller;

import net.freelancertech.base.model.dao.api.util.order.Order;
import net.freelancertech.base.model.dao.api.util.order.OrderContainer;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.view.util.FacesUtils;
import net.freelancertech.base.model.service.api.GroupeServiceLocal;
import net.freelancertech.base.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author jnguetsop
 */
@Named
@SessionScoped
public class GroupeController extends AbstractController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Autowired
    private GroupeServiceLocal groupeService;

    private Groupe groupe;
    private List<Groupe> groupes;
    private List<Groupe> filtereds;

    /**
     * Default constructor
     */
    public GroupeController() {
        groupes = new ArrayList<>(0);
        filtereds = new ArrayList<>(0);
    }

    @PostConstruct
    public void init() {
        initEntity();
    }

    @Override
    public void initEntity() {
        groupe = new Groupe();
    }

    @Override
    public String performCreate() {
        try {
            groupe = groupeService.save(groupe);
            FacesUtils.addInfo("Groupe enrégistré avec succès : " + groupe.getLibelle());
            init();

            return "index" + facesRedirect;
        } catch (Exception exception) {
            String message = ExceptionUtil.getRootErrorMessage(exception,
                    "Echec lors de l'enregistrement.");
            FacesUtils.addError(this.getClass(), exception, message);
        }

        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String edit() {
        if (groupe == null) {
            FacesUtils.addError("Veuillez sélectionner un groupe.");
            return null;
        }

        groupe = groupeService.find(groupe.getId());

        return super.edit();
    }

    @Override
    public String performEdit() {
        try {
            groupe = groupeService.update(groupe);
            if ((groupes != null) && groupes.contains(groupe)) {
                groupes.set(groupes.indexOf(groupe), groupe);
            }

            FacesUtils.addInfo("Le groupe \"" + groupe.getLibelle() + "\" a été modifié avec succès!");
            init();

            return "index" + facesRedirect;
        } catch (Exception exception) {
            String message = ExceptionUtil.getRootErrorMessage(exception,
                    "Update failed. See server log for more information");
            FacesUtils.addError(this.getClass(), exception, message);
        }

        return null;
    }

    @Override
    public String view() {
        if (groupe == null) {
            FacesUtils.addError("Veuillez sélectionner un groupe.");
            return null;
        }

        groupe = groupeService.find(groupe.getId());

        return super.view();
    }

    /**
     *
     * @return
     */
    @Override
    public String delete() {
        if (groupe == null) {
            FacesUtils.addError("Veuillez sélectionner un groupe.");
            return null;
        }

        groupe = groupeService.find(groupe.getId());

        return super.delete();
    }

    /**
     *
     * @return
     */
    @Override
    public String performDelete() {
        try {
            groupeService.delete(groupe);
            FacesUtils.addInfo("Le groupe \"" + groupe.getLibelle() + "\" a été supprimé avec succès.");

            if ((groupes != null) && groupes.contains(groupe)) {
                groupes.remove(groupe);
            }

            init();

            return "index" + facesRedirect;
        } catch (Exception exception) {
            String message = ExceptionUtil.getRootErrorMessage(exception,
                    "Delete failed. See server log for more information");
            FacesUtils.addError(this.getClass(), exception, message);
        }

        return null;
    }

    public String pdfList() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public String excelList() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public String jpeg() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public String pdf() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    //Getters and Setters
    public List<Groupe> getGroupes() {
        OrderContainer oc = OrderContainer.getInstance();
        oc.add("nom", Order.ASC)
                .add("libelle", Order.ASC);
        groupes = groupeService.find(oc);

        return groupes;
    }

    public void setGroupes(List<Groupe> groupes) {
        this.groupes = groupes;
    }

    public Groupe getGroupe() {
        return groupe;
    }

    public void setGroupe(Groupe groupe) {
        this.groupe = groupe;
    }

    public List<Groupe> getFiltereds() {
        return filtereds;
    }

    public void setFiltereds(List<Groupe> filtereds) {
        this.filtereds = filtereds;
    }
}
