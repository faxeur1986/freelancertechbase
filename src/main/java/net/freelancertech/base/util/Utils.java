/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.freelancertech.base.util;

import java.util.Random;

/**
 *
 * @author auguste
 */
public class Utils {
    private static final String[] ALPHABET={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"}; 
   
      
    public static String getAnyCharacter(){
        Random random=new Random();
        int index=random.nextInt();
        if(index<0){
            index=-index;
        }
        index=index%26;
        return ALPHABET[index];
    }
}
