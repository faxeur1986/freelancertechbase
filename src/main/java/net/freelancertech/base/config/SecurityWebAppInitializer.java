/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.config;

import java.util.EnumSet;
import javax.servlet.DispatcherType;
import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * To automatically register springSecurityFilterChain and add context loader
 * listener for SecurityConfig.
 *
 * @author NGUETSOP
 */
@Order(1)
public class SecurityWebAppInitializer extends AbstractSecurityWebApplicationInitializer {

    /**
     * enable the
     * {@link org.springframework.security.web.session.HttpSessionEventPublisher}.
     *
     * @return
     */
    @Override
    protected boolean enableHttpSessionEventPublisher() {
        return true;
    }

    @Override
    protected EnumSet<DispatcherType> getSecurityDispatcherTypes() {
        return EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST, DispatcherType.ERROR, DispatcherType.ASYNC);
    }
}
