/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

/**
 *
 * @author NGUETSOP
 */
@Configuration
public class PersistenceConfig {

    @Bean
    DataSource dataSource() {
        DriverManagerDataSource dmds = new DriverManagerDataSource();
        dmds.setDriverClassName("com.mysql.jdbc.Driver");
        dmds.setUrl("jdbc:mysql://127.0.0.1:3306/freelancertechbase?createDatabaseIfNotExist=true");
        dmds.setUsername("root");
        dmds.setPassword("root");

        return dmds;
    }

    @Bean
    JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hjva = new HibernateJpaVendorAdapter();
        hjva.setDatabase(Database.MYSQL);
        hjva.setShowSql(true);
        hjva.setDatabasePlatform("org.hibernate.dialect.MySQL5InnoDBDialect");
        hjva.setGenerateDdl(true);
        return hjva;
    }

    @Bean
    @DependsOn({"jpaVendorAdapter", "dataSource"})
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean lcemfb = new LocalContainerEntityManagerFactoryBean();
        lcemfb.setPersistenceUnitName("freelancertechbase_pu");
        lcemfb.setDataSource(dataSource());
        lcemfb.setJpaVendorAdapter(jpaVendorAdapter());
        lcemfb.setJpaProperties(additionalProperties());
        return lcemfb;
    }

    @Bean
    @DependsOn("entityManagerFactory")
    JpaTransactionManager transactionManager() {
        JpaTransactionManager jtm = new JpaTransactionManager();
        jtm.setDataSource(dataSource());
        jtm.setEntityManagerFactory(entityManagerFactory().getObject());

        return jtm;
    }

    @Bean
    PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor() {
        PersistenceAnnotationBeanPostProcessor pabpp = new PersistenceAnnotationBeanPostProcessor();
        pabpp.setResourceRef(false);
        return pabpp;
    }

    @Bean
    PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        PersistenceExceptionTranslationPostProcessor petpp = new PersistenceExceptionTranslationPostProcessor();
        return petpp;
    }

    final Properties additionalProperties() {
        final Properties properties = new Properties();
        properties.setProperty("hibernate.format_sql", "true");
        return properties;
    }
}
