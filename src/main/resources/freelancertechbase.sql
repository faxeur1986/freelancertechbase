/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.0.24-community-nt : Database - freelancertechbase
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`freelancertechbase` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `freelancertechbase`;

/*Table structure for table `groupe` */

DROP TABLE IF EXISTS `groupe`;

CREATE TABLE `groupe` (
  `id` bigint(20) NOT NULL auto_increment,
  `date_cre` datetime NOT NULL,
  `date_maj` datetime NOT NULL,
  `user_cre` varchar(255) NOT NULL,
  `user_maj` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `actif` bit(1) NOT NULL,
  `description` varchar(255) default NULL,
  `libelle` varchar(60) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `parent` varchar(60) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UK_b30fmcjbp89j49uc6qtdb4dni` (`nom`),
  UNIQUE KEY `UK_q8po4f0c4cl44aya0e8pa286g` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `groupe` */

insert  into `groupe`(`id`,`date_cre`,`date_maj`,`user_cre`,`user_maj`,`version`,`actif`,`description`,`libelle`,`nom`,`parent`) values (1,'2014-08-30 12:21:50','2014-08-30 12:21:57','admin','admin',0,'','Groupe des administrateurs','Administrateur','ROLE_ADMIN',NULL),(2,'2014-08-30 12:22:45','2014-08-30 12:22:48','admin','admin',0,'','Groupe des utilisateurs','Utilisateur','ROLE_USER',NULL);

/*Table structure for table `photo` */

DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo` (
  `id` bigint(20) NOT NULL auto_increment,
  `date_cre` datetime NOT NULL,
  `date_maj` datetime NOT NULL,
  `user_cre` varchar(255) NOT NULL,
  `user_maj` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `contenu` longblob NOT NULL,
  `description` longtext,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `photo` */

/*Table structure for table `utilisateur` */

DROP TABLE IF EXISTS `utilisateur`;

CREATE TABLE `utilisateur` (
  `id` bigint(20) NOT NULL auto_increment,
  `date_cre` datetime NOT NULL,
  `date_maj` datetime NOT NULL,
  `user_cre` varchar(255) NOT NULL,
  `user_maj` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `expired` bit(1) NOT NULL,
  `login` varchar(30) NOT NULL,
  `niveau` int(11) default NULL,
  `nom` varchar(60) NOT NULL,
  `poste` varchar(60) default NULL,
  `pwd` varchar(255) NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UK_18vwp4resqussqmlpqnymfqxk` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `utilisateur` */

insert  into `utilisateur`(`id`,`date_cre`,`date_maj`,`user_cre`,`user_maj`,`version`,`enabled`,`expired`,`login`,`niveau`,`nom`,`poste`,`pwd`,`status`) values (1,'2014-08-30 00:00:00','2014-11-18 02:46:08','admin','admin',1,'','\0','admin',1,'NGUETSOP KOMOLO Jere','KOMOLO-PC','$2a$08$nAphjvKn1x24e.6O0Wnye.Bqwa612uOFMeIh2ePYAwRgDTGFv.dDi',''),(2,'2014-09-10 19:18:46','2014-11-18 02:45:41','admin','admin',1,'','\0','jnguetsop',4,'NGUETSOP KOMOLO Jeremie','KOMOLO-PC','$2a$08$0PDiS2Curvsfilitlp0Ebe2F1cTZfyXuY1cOXH8l7vCI.5lgumopG',''),(3,'2014-11-18 02:48:45','2014-11-18 03:48:22','admin','admin',1,'','\0','fouomene',10,'fouomene','Team Leader','$2a$08$xwRYOYMWBAnThrTIHWgw3eLSqAzO1.Gr6eeAYyqjG7uMCFp4MytaC','');

/*Table structure for table `utilisateur_groupe` */

DROP TABLE IF EXISTS `utilisateur_groupe`;

CREATE TABLE `utilisateur_groupe` (
  `id` bigint(20) NOT NULL auto_increment,
  `date_cre` datetime NOT NULL,
  `date_maj` datetime NOT NULL,
  `user_cre` varchar(255) NOT NULL,
  `user_maj` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `actif` bit(1) NOT NULL,
  `force_` varchar(255) default NULL,
  `id_groupe` bigint(20) NOT NULL,
  `id_utilisateur` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UK_bk0bin3x3y0mue9bilmm02ojx` (`id_utilisateur`,`id_groupe`),
  KEY `FK_2ljmoim3h1tkr852g9hf05n0k` (`id_groupe`),
  CONSTRAINT `FK_2ljmoim3h1tkr852g9hf05n0k` FOREIGN KEY (`id_groupe`) REFERENCES `groupe` (`id`),
  CONSTRAINT `FK_9vi62o6p6x3aihmbq5xtm1pw0` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `utilisateur_groupe` */

insert  into `utilisateur_groupe`(`id`,`date_cre`,`date_maj`,`user_cre`,`user_maj`,`version`,`actif`,`force_`,`id_groupe`,`id_utilisateur`) values (1,'2014-08-30 12:23:48','2014-08-30 12:23:51','admin','admin',0,'','\0',1,1),(2,'2014-08-30 12:24:31','2014-08-30 12:24:34','admin','admin',0,'','\0',2,1),(3,'2014-09-10 19:18:46','2014-09-10 19:18:46','admin','admin',0,'','',1,2),(4,'2014-09-10 19:18:46','2014-09-10 19:18:46','admin','admin',0,'','',2,2),(5,'2014-11-18 02:48:46','2014-11-18 02:48:46','admin','admin',0,'','',1,3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
